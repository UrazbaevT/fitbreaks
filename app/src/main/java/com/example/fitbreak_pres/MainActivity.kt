package com.example.fitbreak_pres

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.fitbreak_pres.data.local.Pref
import com.example.fitbreak_pres.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var pref: Pref
    private var currentUser: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        pref = Pref(this)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        navController.navigate(R.id.splashScreenFragment)

//        if (currentUser){
//            navController.navigate(R.id.signInFragment)
//        }else{
//            navController.navigate(R.id.onBoardingFragment)
//        }
//        navController.navigate(R.id.onBoardingFragment)

        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_for_you,
                R.id.navigation_warm_up,
                R.id.navigation_profile,
                R.id.onBoardingFragment,
                R.id.signUpFragment,
                R.id.signInFragment,
                R.id.splashScreenFragment,
                R.id.describeFragment
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        val bottomNavFragments =
            arrayListOf(R.id.navigation_for_you, R.id.navigation_warm_up, R.id.navigation_profile)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            navView.isVisible = bottomNavFragments.contains(destination.id)
        }
    }
}