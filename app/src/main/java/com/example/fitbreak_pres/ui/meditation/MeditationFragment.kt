package com.example.fitbreak_pres.ui.meditation

import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.data.model.Meditation
import com.example.fitbreak_pres.databinding.FragmentMeditationBinding
import com.example.fitbreak_pres.ui.meditation.adapter.MeditationAdapter
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.io.IOException

class MeditationFragment : BaseFragment<FragmentMeditationBinding, MeditationViewModel>() {

    private lateinit var adapter: MeditationAdapter
    private lateinit var dbref: DatabaseReference
    private lateinit var list: ArrayList<Meditation>
    private var mediaPlayer: MediaPlayer? = null

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentMeditationBinding {
        return FragmentMeditationBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): MeditationViewModel {
        return ViewModelProvider(requireActivity())[MeditationViewModel::class.java]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tvNameWarmUp.text = arguments?.getString("title_name")
    }

    override fun initView() {
        super.initView()

        binding.rvMeditation.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        binding.rvMeditation.setHasFixedSize(true)

        list = arrayListOf<Meditation>()
        list.clear()
        getSongs()
        adapter = MeditationAdapter(list, this::onPlay, this::onPause)
    }

    override fun initListener() {
        super.initListener()

        binding.tvBack.setOnClickListener {
            findNavController().navigateUp()
        }

    }

    private fun getSongs() {
        dbref = FirebaseDatabase.getInstance().getReference("Meditation")

        dbref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (userSnapshot in snapshot.children) {
                        val song = userSnapshot.getValue(Meditation::class.java)
                        list.add(song!!)
                    }
                    binding.rvMeditation.adapter = adapter
                }
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }

    private fun onPlay(meditation: Meditation) {
        val link = meditation.link

        mediaPlayer = MediaPlayer()
        mediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
        try {
            mediaPlayer!!.setDataSource(link)
            mediaPlayer!!.prepare()
            mediaPlayer!!.start()
        }catch (e: IOException){
            e.printStackTrace()
        }
        Toast.makeText(requireContext(), "Audio started playing!", Toast.LENGTH_SHORT).show()
    }

    private fun onPause(meditation: Meditation) {
        if (mediaPlayer!!.isPlaying){
            mediaPlayer!!.stop()
            mediaPlayer!!.reset()
            mediaPlayer!!.release()
        }else{
            Toast.makeText(requireContext(), "Audio has not played", Toast.LENGTH_SHORT).show()
        }
    }

}