package com.example.fitbreak_pres.ui.describe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import coil.load
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.data.model.WarmUp
import com.example.fitbreak_pres.databinding.FragmentDescribeBinding

class DescribeFragment : BaseFragment<FragmentDescribeBinding, DescribeViewModel>() {

    private var warmUp = WarmUp()
    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentDescribeBinding {
        return FragmentDescribeBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): DescribeViewModel {
        return ViewModelProvider(requireActivity())[DescribeViewModel::class.java]
    }

    override fun initView() {
        super.initView()
        binding.tvNameWarmUp.text = arguments?.getString("title_name")

        val res = arguments?.getSerializable("detail") as? WarmUp

        binding.tvTitleExerciseLl1.text = res?.detail?.title1
        binding.tvTitleExerciseLl2.text = res?.detail?.title2

        binding.tvDescExerciseLl1.text = res?.detail?.desc1
        binding.tvDescExerciseLl2.text = res?.detail?.desc2

        binding.tvHowManyTimesLl1.text = res?.detail?.howMany1
        binding.tvHowManyTimesLl2.text = res?.detail?.howMany2

        binding.imgGifLl1.load(res?.detail?.image1)
        binding.imgGifLl2.load(res?.detail?.image2)

        binding.btnStart.setOnClickListener {
            if (binding.btnStart.isClickable) {
                val bundle = Bundle()
                bundle.putString("title1", binding.tvTitleExerciseLl1.text.toString())
                bundle.putString("title2", binding.tvTitleExerciseLl2.text.toString())
                bundle.putString("howMany1", binding.tvHowManyTimesLl1.text.toString())
                bundle.putString("howMany2", binding.tvHowManyTimesLl2.text.toString())
                bundle.putString("img1", binding.imgGifLl1.load(res?.detail?.image1).toString())
                bundle.putString("img2", binding.imgGifLl2.load(res?.detail?.image2).toString())
                findNavController().navigate(R.id.exerciseFragment, bundle)
            }
        }
    }

    override fun initListener() {
        super.initListener()
        binding.tvBack.setOnClickListener {
            findNavController().navigateUp()
        }

    }

}