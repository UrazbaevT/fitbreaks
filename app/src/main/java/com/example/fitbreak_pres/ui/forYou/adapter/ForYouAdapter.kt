package com.example.fitbreak_pres.ui.forYou.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.fitbreak_pres.data.model.WarmUp
import com.example.fitbreak_pres.databinding.ItemWarmUpBinding

class ForYouAdapter(private var list: ArrayList<WarmUp>, private val onClick: (WarmUp) -> Unit) :
    RecyclerView.Adapter<ForYouAdapter.ForYouViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForYouViewHolder {
        return ForYouViewHolder(
            ItemWarmUpBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ForYouViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount() = list.size

    inner class ForYouViewHolder(private val binding: ItemWarmUpBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(warmUp: WarmUp?) {
            binding.imgItem.load(warmUp?.image)
            binding.tvTitle.text = warmUp?.title

            itemView.setOnClickListener {
                onClick(warmUp!!)
            }
        }
    }
}