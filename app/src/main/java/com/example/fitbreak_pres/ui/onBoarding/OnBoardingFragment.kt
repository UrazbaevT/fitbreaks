package com.example.fitbreak_pres.ui.onBoarding

import androidx.lifecycle.ViewModelProvider
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.data.local.Pref
import com.example.fitbreak_pres.databinding.FragmentOnBoardingBinding
import com.example.fitbreak_pres.ui.onBoarding.adapter.OnBoardingAdapter

class OnBoardingFragment : BaseFragment<FragmentOnBoardingBinding, OnBoardingViewModel>() {

    private lateinit var pref: Pref
    private lateinit var adapter: OnBoardingAdapter

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOnBoardingBinding {
        return FragmentOnBoardingBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): OnBoardingViewModel {
        return ViewModelProvider(requireActivity())[OnBoardingViewModel::class.java]
    }

    override fun initView() {
        super.initView()
        pref = Pref(requireContext())
        adapter = OnBoardingAdapter(this::onClick)
        binding.viewPager2.adapter = adapter
    }

    private fun onClick() {
        if (binding.viewPager2.currentItem == 1) {
            pref.saveSeen()
            findNavController().navigate(R.id.navigation_warm_up)
        } else {
            binding.viewPager2.currentItem += 1
        }
    }
}