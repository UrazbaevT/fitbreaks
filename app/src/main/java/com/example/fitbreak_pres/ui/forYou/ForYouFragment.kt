package com.example.fitbreak_pres.ui.forYou

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.data.model.WarmUp
import com.example.fitbreak_pres.databinding.FragmentForYouBinding
import com.example.fitbreak_pres.ui.forYou.adapter.ForYouAdapter
import com.example.fitbreak_pres.ui.warm_up.adapter.WarmUpAdapter
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ForYouFragment : BaseFragment<FragmentForYouBinding, ForYouViewModel>() {

    private lateinit var adapter: ForYouAdapter
    private lateinit var dbref: DatabaseReference
    private lateinit var list: ArrayList<WarmUp>

    override fun inflateBinding(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentForYouBinding {
        return FragmentForYouBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): ForYouViewModel {
        return ViewModelProvider(requireActivity())[ForYouViewModel::class.java]
    }

    override fun initView() {
        super.initView()

        binding.rvForYou.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        binding.rvForYou.setHasFixedSize(true)

        list = arrayListOf<WarmUp>()
        list.clear()
        getForYou()
        adapter = ForYouAdapter(list, this::onClick)

    }

    private fun getForYou() {
        dbref = FirebaseDatabase.getInstance().getReference("WarmUp")

        dbref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (userSnapshot in snapshot.children) {
                        val user = userSnapshot.getValue(WarmUp::class.java)
                        list.add(user!!)
                    }
                    binding.rvForYou.adapter = adapter
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })
    }

    private fun onClick(warmUp: WarmUp) {
        val bundle = Bundle()
        bundle.putString("title_name", warmUp.title)
        bundle.putSerializable("detail", warmUp)
        findNavController().navigate(R.id.describeFragment, bundle)
    }
}