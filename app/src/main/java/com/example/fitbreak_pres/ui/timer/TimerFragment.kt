package com.example.fitbreak_pres.ui.timer

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import coil.load
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.databinding.FragmentTimerBinding
import org.koin.android.ext.android.bind

class TimerFragment : BaseFragment<FragmentTimerBinding, TimerViewModel>() {

    private lateinit var countDownTimer: CountDownTimer
    private val initialCountdown: Long = 20000 // 20 seconds
    private val countdownInterval: Long = 1000 // 1 second
    private var transitionCount: Int = 0

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentTimerBinding {
        return FragmentTimerBinding.inflate(
            inflater, container, false
        )
    }

    override fun viewModel(): TimerViewModel {
        return ViewModelProvider(requireActivity())[TimerViewModel::class.java]
    }

    override fun initView() {
        super.initView()
        startCountdown()

    }

    private fun startCountdown() {

        val title2 = arguments?.getString("title2")
        val howMany2 = arguments?.getString("howMany2")
        val img2 = arguments?.getString("img2")
        binding.tvTitle2.text = title2
        binding.imgGif2.load(img2)

        countDownTimer = object : CountDownTimer(initialCountdown, countdownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                val minutes = millisUntilFinished / 1000 / 60
                val seconds = (millisUntilFinished / 1000) % 60
                val timeLeftFormatted = String.format("%02d:%02d", minutes, seconds)
                binding.timer.text = timeLeftFormatted
            }

            override fun onFinish() {
                binding.timer.text = "00:00"
//                if (transitionCount < 1) {
//                    transitionCount++
//                } else {}
                    val bundle = Bundle()
                    bundle.putString("title2", title2)
                    bundle.putString("img2", img2)
                    bundle.putString("howMany2", howMany2)
                    findNavController().navigate(R.id.lastExerciseFragment, bundle)
            }
        }
        countDownTimer.start()

        binding.btnNext.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("title2", title2)
            bundle.putString("img2", img2)
            bundle.putString("howMany2", howMany2)
            findNavController().navigate(R.id.lastExerciseFragment, bundle)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        countDownTimer.cancel()
    }

}
