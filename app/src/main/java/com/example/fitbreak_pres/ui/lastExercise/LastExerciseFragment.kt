package com.example.fitbreak_pres.ui.lastExercise

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import coil.load
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.databinding.FragmentLastExerciseBinding
import org.koin.android.ext.android.bind

class LastExerciseFragment : BaseFragment<FragmentLastExerciseBinding, LastExerciseViewModel>() {
    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentLastExerciseBinding {
        return FragmentLastExerciseBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): LastExerciseViewModel {
        return ViewModelProvider(requireActivity())[LastExerciseViewModel::class.java]
    }

    override fun initView() {
        super.initView()

        val title2 = arguments?.getString("title2")
        val img2 = arguments?.getString("img2")
        val howMany2 = arguments?.getString("howMany2")

        binding.tvTitle.text = title2
        binding.tvHowMany.text = howMany2
        binding.imgGif.load(img2)

        binding.btnDone.setOnClickListener {
            findNavController().navigate(R.id.resultFragment)
        }
    }

}