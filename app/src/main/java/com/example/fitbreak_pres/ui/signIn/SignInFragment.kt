package com.example.fitbreak_pres.ui.signIn

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.data.local.Pref
import com.example.fitbreak_pres.databinding.FragmentSignInBinding
import com.google.firebase.auth.FirebaseAuth

class SignInFragment : BaseFragment<FragmentSignInBinding, SignInViewModel>() {

    private lateinit var pref: Pref
    private lateinit var firebaseAuth: FirebaseAuth

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSignInBinding {
        return FragmentSignInBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): SignInViewModel {
        return ViewModelProvider(requireActivity())[SignInViewModel::class.java]
    }

    override fun initView() {
        super.initView()
        pref = Pref(requireContext())

        firebaseAuth = FirebaseAuth.getInstance()
    }

    override fun initListener() {
        super.initListener()
        binding.tvCreateAccount.setOnClickListener {
//            if (!pref.isUserSeen())
            findNavController().navigate(R.id.signUpFragment)
        }

        binding.btnSignIn.setOnClickListener {
            if (validateFields()) {
                val email = binding.etEmailSignIn.text.toString()
                val pass = binding.etPasswordSignIn.text.toString()

                if (email.isNotEmpty() && pass.isNotEmpty()) {

                    firebaseAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener {
                        if (it.isSuccessful) {
                            findNavController().navigate(R.id.navigation_warm_up)
                        } else {
                            Toast.makeText(
                                requireContext(),
                                "Неверный пароль или логин!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } else {
                    Toast.makeText(requireContext(), "Заполните поля!", Toast.LENGTH_SHORT).show()
                }
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }

    }

    private fun validateFields(): Boolean {
        val email = binding.etEmailSignIn.text.toString().trim()
        val password = binding.etPasswordSignIn.text.toString().trim()

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(requireContext(), "Пожалуйста, заполните все поля", Toast.LENGTH_SHORT)
                .show()
            return false
        }

        val emailPattern = "[a-zA-Z0-9._-]+@gmail.com"
        if (!email.matches(emailPattern.toRegex())) {
            binding.etEmailSignIn.error = "Пожалуйста, введите действительный адрес Gmail"
            return false
        }

        if (password.length < 8) {
            Toast.makeText(
                requireContext(),
                "Пароль должен содержать не менее 8 символов",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }

        return true
    }

//    override fun onStart() {
//        super.onStart()
//        if (firebaseAuth.currentUser != null) {
//           findNavController().navigate(R.id.navigation_warm_up)
//        }
//    }

}