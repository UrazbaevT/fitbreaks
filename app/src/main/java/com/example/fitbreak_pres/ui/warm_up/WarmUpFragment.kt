package com.example.fitbreak_pres.ui.warm_up

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.data.model.WarmUp
import com.example.fitbreak_pres.databinding.FragmentWarmUpBinding
import com.example.fitbreak_pres.ui.warm_up.adapter.WarmUpAdapter
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class WarmUpFragment : BaseFragment<FragmentWarmUpBinding, WarmUpViewModel>() {

    private lateinit var adapter: WarmUpAdapter
    private lateinit var dbref: DatabaseReference
    private lateinit var list: ArrayList<WarmUp>

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentWarmUpBinding {
        return FragmentWarmUpBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): WarmUpViewModel {
        return ViewModelProvider(requireActivity())[WarmUpViewModel::class.java]
    }

    override fun initView() {
        super.initView()

        binding.rvWarmUp.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        binding.rvWarmUp.setHasFixedSize(true)

        list = arrayListOf<WarmUp>()
        list.clear()
        getUserData()
        adapter = WarmUpAdapter(list, this::onClick, this::onMeditationClick)
    }

    private fun getUserData() {
        dbref = FirebaseDatabase.getInstance().getReference("WarmUp")

        dbref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (userSnapshot in snapshot.children) {
                        val user = userSnapshot.getValue(WarmUp::class.java)
                        list.add(user!!)
                    }
                    binding.rvWarmUp.adapter = adapter
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })
    }

    private fun onClick(warmUp: WarmUp) {
        val bundle = Bundle()
        bundle.putString("title_name", warmUp.title)
        bundle.putSerializable("detail", warmUp)
        findNavController().navigate(R.id.describeFragment, bundle)
    }

    private fun onMeditationClick(warmUp: WarmUp){
        findNavController().navigate(R.id.meditationFragment, bundleOf("title_name" to warmUp.title))
    }

    override fun initListener() {
        super.initListener()
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }
    }
}