package com.example.fitbreak_pres.ui.meditation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.fitbreak_pres.data.model.Meditation
import com.example.fitbreak_pres.databinding.ItemMeditationBinding

class MeditationAdapter(
    private val list: ArrayList<Meditation>,
    private val onPlay: (Meditation) -> Unit,
    private val onPause: (Meditation) -> Unit
) : RecyclerView.Adapter<MeditationAdapter.MeditationViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeditationViewHolder {
        return MeditationViewHolder(
            ItemMeditationBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: MeditationViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class MeditationViewHolder(private val binding: ItemMeditationBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(meditation: Meditation?) {
            binding.tvAuthor.text = meditation?.author
            binding.tvSong.text = meditation?.song
            binding.imgMedit.load(meditation?.image)

            val isPlaying = meditation?.isPlaying ?: false
            val isCurrentSongPlaying = meditation?.isCurrentSongPlaying ?: false

            if (isPlaying && isCurrentSongPlaying) {
                binding.ivPlay.visibility = View.GONE
                binding.ivPause.visibility = View.VISIBLE
            } else {
                binding.ivPlay.visibility = View.VISIBLE
                binding.ivPause.visibility = View.GONE
            }

            binding.ivPlay.setOnClickListener {
                if (isPlaying && isCurrentSongPlaying) {
                    return@setOnClickListener
                }

                stopPreviousSong()

                meditation?.isCurrentSongPlaying = true

                binding.ivPlay.visibility = View.GONE
                binding.ivPause.visibility = View.VISIBLE
                onPlay(meditation!!)
                playCurrentSong()

            }

            binding.ivPause.setOnClickListener {
                stopCurrentSong()

                meditation?.isCurrentSongPlaying = false

                binding.ivPlay.visibility = View.VISIBLE
                binding.ivPause.visibility = View.GONE
                onPause(meditation!!)
            }

        }

        private fun stopCurrentSong() {
//            if (currentMediaPlayer != null && currentMediaPlayer.isPlaying) {
//                currentMediaPlayer.stop()
//            }
        }

        private fun playCurrentSong() {
//            currentMediaPlayer = MediaPlayer()
//            currentMediaPlayer.setDataSource(meditation?.link)
//            currentMediaPlayer.prepare()
//            currentMediaPlayer.start()
        }

        private fun stopPreviousSong() {
//            if (previousMediaPlayer != null && previousMediaPlayer.isPlaying) {
//                previousMediaPlayer.stop()
//            }
        }
    }
}