package com.example.fitbreak_pres.ui.profile

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.core.utils.loadImage
import com.example.fitbreak_pres.data.local.Pref
import com.example.fitbreak_pres.data.local.SharedPref
import com.example.fitbreak_pres.data.model.User
import com.example.fitbreak_pres.databinding.FragmentProfileBinding
import com.google.android.material.button.MaterialButton
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileViewModel>() {

    private lateinit var pref: Pref
    private lateinit var sharedPref: SharedPref
    private var isCardViewVisible = false

    private val launcher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK && result.data != null) {
                val photoUri = result.data?.data
                pref.saveImage(photoUri.toString())
                binding.imgProfile.loadImage(photoUri.toString())
            }
        }

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentProfileBinding {
        return FragmentProfileBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): ProfileViewModel {
        return ViewModelProvider(requireActivity())[ProfileViewModel::class.java]
    }

    override fun initView() {
        super.initView()
        pref = Pref(requireContext())
        sharedPref = SharedPref(requireContext())
        binding.imgProfile.loadImage(pref.getImage())
        binding.tvAgeInput.text = pref.getAge()
        binding.tvLoginInput.text = pref.getLogin()
        binding.tvSexInput.text = pref.getRadioButton()
        binding.tvFirstName.text = pref.getFirstName()
        binding.tvLastName.text = pref.getLastName()

        setData()
    }

    private fun setData() {
        val database = FirebaseDatabase.getInstance()
        val userReference = database.getReference("user")
        val userProfile = User(firstName = sharedPref.isFistName(), lastName = sharedPref.isLastName(), gender = sharedPref.isGender(), age = sharedPref.isAge(), email = sharedPref.isEmail())
        val userId = userReference.push().key
        if (userId != null) {
            userReference.child(userId).setValue(userProfile)
        }

        if (userId != null) {
            userReference.child(userId).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val userProfile = snapshot.getValue(User::class.java)

                    if (userProfile != null){
                        binding.tvAgeInput.text = userProfile.age
                        binding.tvFirstName.text = userProfile.firstName
                        binding.tvLastName.text = userProfile.lastName
                        binding.tvSexInput.text = userProfile.gender
                        binding.tvLoginInput.text = userProfile.email
                    }
                }
                override fun onCancelled(error: DatabaseError) {
                    Toast.makeText(requireContext(), "Произошла ошибка!!", Toast.LENGTH_SHORT).show()
                }

            })
        }
    }

    override fun initListener() {
        super.initListener()
        binding.tvEditImg.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            launcher.launch(intent)
        }

        binding.imgBack.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.btnEditData.setOnClickListener {
            customAlertDialog()
        }

        binding.imgEditName.setOnClickListener {
            customAlertDialogForFullName()
        }

        binding.imgDown.setOnClickListener {
            if (isCardViewVisible) {
                binding.cardView.visibility = View.GONE
                isCardViewVisible = false
            } else {
                binding.cardView.visibility = View.VISIBLE
                isCardViewVisible = true
            }
        }
    }

    private fun customAlertDialog() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_alert_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val radioGroup: RadioGroup = dialog.findViewById(R.id.radio_group)
        val etAge: EditText = dialog.findViewById(R.id.et_age)
        val etEmailOrNumber: EditText = dialog.findViewById(R.id.et_email_or_number)
        val btnDone: MaterialButton = dialog.findViewById(R.id.btn_done)
        val tvCancel: TextView = dialog.findViewById(R.id.tv_cancel)

        btnDone.setOnClickListener {
            val age = etAge.text.toString()
            val login = etEmailOrNumber.text.toString().trim()
            val radioButtonId = radioGroup.checkedRadioButtonId

            if (age.isEmpty() || login.isEmpty() || radioButtonId == -1) {
                Toast.makeText(requireContext(), "Заполните все поля", Toast.LENGTH_SHORT).show()
            } else {
                val ageValue = age.toIntOrNull()

                if (ageValue != null && ageValue >= 10 && ageValue <= 90) {
                    binding.tvAgeInput.text = age

                    if (login.endsWith("gmail.com") && !login.contains(" ")) {
                        binding.tvLoginInput.text = login

                        val radio: RadioButton = dialog.findViewById(radioButtonId)
                        binding.tvSexInput.text = radio.text.toString()

                        pref.saveAge(age)
                        pref.saveLogin(login)
                        pref.saveRadioButton(radio.text.toString())

                        dialog.dismiss()

                        checkMark()
                    } else {
                        etEmailOrNumber.setError("Введите gmail в формате example@gmail.com")
                    }
                } else {
                    etAge.setError("Введите значение от 10 до 90")
                }
            }
        }
        tvCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun checkMark() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.toast_custom)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.show()

        val handler = android.os.Handler()
        handler.postDelayed({
            dialog.dismiss()
        }, 1000)

    }

    private fun customAlertDialogForFullName() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_alert_dialog_for_full_name)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val btnDoneName: MaterialButton = dialog.findViewById(R.id.btn_done_name)
        val etFirstName: EditText = dialog.findViewById(R.id.et_first_name_custom)
        val etLastName: EditText = dialog.findViewById(R.id.et_last_name_custom)
        val tvCancelFullName: TextView = dialog.findViewById(R.id.tv_cancel_for_full_name)

        btnDoneName.setOnClickListener {
            val first = etFirstName.text.toString().trim()
            val last = etLastName.text.toString().trim()

            if (first.isEmpty() || last.isEmpty()) {
                Toast.makeText(requireContext(), "Заполните поля", Toast.LENGTH_SHORT).show()
            } else if (!first[0].isUpperCase() || !last[0].isUpperCase()) {
                Toast.makeText(
                    requireContext(),
                    "Имя и фамилия должны начинаться с заглавной буквы",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    requireContext(),
                    "Имя: $first, Фамилия: $last",
                    Toast.LENGTH_SHORT
                ).show()

                pref.saveFirstName(first)
                pref.saveLastName(last)

                binding.tvFirstName.text = first
                binding.tvLastName.text = last

                dialog.dismiss()
            }
        }
        tvCancelFullName.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


}