package com.example.fitbreak_pres.ui.exercise

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import coil.load
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.databinding.FragmentExerciseBinding

class ExerciseFragment : BaseFragment<FragmentExerciseBinding, ExerciseViewModel>() {

//    private var transitionCount: Int = 0

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentExerciseBinding {
        return FragmentExerciseBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): ExerciseViewModel {
        return ViewModelProvider(requireActivity())[ExerciseViewModel::class.java]
    }

    override fun initView() {
        super.initView()
        val title1 = arguments?.getString("title1")
        val title2 = arguments?.getString("title2")

        val howMany1 = arguments?.getString("howMany1")
        val howMany2 = arguments?.getString("howMany2")

        val img1 = arguments?.getString("img1")
        val img2 = arguments?.getString("img2")

        binding.tvTitle.text = title1.toString()
        binding.tvHowMany.text = howMany1.toString()
        binding.imgGif.load(img1)

        binding.btnDone.setOnClickListener {
//            if (transitionCount < 1) {
//                transitionCount++
//            } else {

            val bundle = Bundle()
            bundle.putString("title2", title2)
            bundle.putString("howMany2", howMany2)
            bundle.putString("img2", img2)
            findNavController().navigate(R.id.timerFragment, bundle)
        }
    }
}