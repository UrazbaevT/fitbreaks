package com.example.fitbreak_pres.ui.result

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.databinding.FragmentResultBinding
import nl.dionsegijn.konfetti.KonfettiView
import nl.dionsegijn.konfetti.models.Shape
import nl.dionsegijn.konfetti.models.Size

class ResultFragment : BaseFragment<FragmentResultBinding, ResultViewModel>() {
    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentResultBinding {
        return FragmentResultBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): ResultViewModel {
        return ViewModelProvider(requireActivity())[ResultViewModel::class.java]
    }

    override fun initView() {
        super.initView()
        val konfettiView: KonfettiView = binding.konf
        konfettiView.build().addColors(Color.YELLOW, Color.BLUE, Color.MAGENTA)
            .setDirection(0.0, 359.0)
            .setSpeed(3f, 8f)
            .setFadeOutEnabled(true)
            .setTimeToLive(1000L)
            .addShapes(Shape.Square, Shape.Circle)
            .addSizes(Size(12))
            .setPosition(+50f, konfettiView.width + 50f, -50f, -50f)
            .streamFor(900, 4000L)
    }

    override fun initListener() {
        super.initListener()
        binding.btnDoneResult.setOnClickListener {
            findNavController().navigate(R.id.navigation_warm_up)
        }
    }

}