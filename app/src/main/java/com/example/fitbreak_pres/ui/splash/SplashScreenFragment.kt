package com.example.fitbreak_pres.ui.splash

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.databinding.FragmentSplashScreenBinding
import com.google.firebase.auth.FirebaseAuth

@SuppressLint("CustomSplashScreen")
class SplashScreenFragment : BaseFragment<FragmentSplashScreenBinding, SplashScreenViewModel>() {

    private val currentUser: Boolean = true
    private lateinit var firebaseAuth: FirebaseAuth

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSplashScreenBinding {
        return FragmentSplashScreenBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): SplashScreenViewModel {
        return ViewModelProvider(this)[SplashScreenViewModel::class.java]
    }


    override fun initView() {
        super.initView()
//        pref = Pref(requireContext())
        firebaseAuth = FirebaseAuth.getInstance()

        binding.imgSplash.startAnimation(
            AnimationUtils.loadAnimation(
                requireContext(),
                R.anim.slide
            )
        )

        view?.postDelayed({
            if (firebaseAuth.currentUser != null) {
                findNavController().navigate(R.id.navigation_warm_up)
            } else {
                findNavController().navigate(R.id.signInFragment)
            }
        }, 3000)


//        val animation = AlphaAnimation(0f, 1f).apply {
//            duration = 4000
//            setAnimationListener(object : Animation.AnimationListener {
//                override fun onAnimationStart(animation: Animation?) {}
//                override fun onAnimationEnd(animation: Animation?) {
//                    // Выполняется после окончания анимации
//                    // Здесь вы можете запустить основную активность или выполнить другие действия
//                    findNavController().navigate(R.id.signInFragment)
//                }
//
//                override fun onAnimationRepeat(animation: Animation?) {}
//            })
//        }

//        binding.imgSplash.startAnimation(animation)
    }
}