package com.example.fitbreak_pres.ui.onBoarding.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.data.model.OnBoard
import com.example.fitbreak_pres.databinding.ItemOnBoardingBinding

class OnBoardingAdapter(private val onClick: () -> Unit) :
    RecyclerView.Adapter<OnBoardingAdapter.OnBoardingViewHolder>() {

    private var data = arrayListOf(
        OnBoard(
            title = "Какую цель Вы хотите\nдостичь с помощью\nFitBreak",
            desc = "",
            img = R.drawable.img_target,
            radioFirst = "Бодрость и продуктивность",
            radioSecond = "Устранение усталости",
            radioThird = "Улучшение фокусировки",
            radioFourth = "Разминка суставов и мышц",
            radioFifth = "",
            btnRadioFifth = false
        ),
        OnBoard(
            title = "Выберите ваш график",
            desc = "Это поможет нам вовремя\nсообщать вам о том что пора\nсделать разминку",
            img = R.drawable.img_notifi,
            radioFirst = "9:00-18:00 - обед 12:00-13:00",
            radioSecond = "8:30-17:30 - обед 12:00-13:00",
            radioThird = "8:00-17:00 - обед 12:00-13:00",
            radioFourth = "10:00-22:00 - бед обеда",
            radioFifth = "ненормированный / другой",
            btnRadioFifth = true
        )
    )

    private val selectedRadioIndices = ArrayList<Int?>(data.size)

    init {
        repeat(data.size) { selectedRadioIndices.add(null) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnBoardingViewHolder {
        return OnBoardingViewHolder(
            ItemOnBoardingBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: OnBoardingViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class OnBoardingViewHolder(private val binding: ItemOnBoardingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(onBoard: OnBoard) {
            with(binding) {
                tvTitle.text = onBoard.title
                tvDesc.text = onBoard.desc
                ivImage.setImageResource(onBoard.img)
                radioFirst.text = onBoard.radioFirst
                radioSecond.text = onBoard.radioSecond
                radioThird.text = onBoard.radioThird
                radioFourth.text = onBoard.radioFourth
                radioFifth.text = onBoard.radioFifth
                radioFifth.isVisible = onBoard.btnRadioFifth

                radioGroup.setOnCheckedChangeListener { _, checkedId ->
                    selectedRadioIndices[adapterPosition] =
                        radioGroup.indexOfChild(radioGroup.findViewById(checkedId))
                }
            }

            binding.btnNext.setOnClickListener {
                if (selectedRadioIndices[adapterPosition] != null) {
                    onClick()
                } else {
                    Toast.makeText(
                        binding.root.context,
                        "Please select an option",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

}
