package com.example.fitbreak_pres.ui.signUp

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.fitbreak_pres.R
import com.example.fitbreak_pres.core.ui.BaseFragment
import com.example.fitbreak_pres.data.local.SharedPref
import com.example.fitbreak_pres.databinding.FragmentSignUpBinding
import com.google.firebase.auth.FirebaseAuth

class SignUpFragment : BaseFragment<FragmentSignUpBinding, SignUpViewModel>() {

    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var pref: SharedPref

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSignUpBinding {
        return FragmentSignUpBinding.inflate(inflater, container, false)
    }

    override fun viewModel(): SignUpViewModel {
        return ViewModelProvider(requireActivity())[SignUpViewModel::class.java]
    }

    override fun initView() {
        super.initView()

        firebaseAuth = FirebaseAuth.getInstance()
        pref = SharedPref(requireContext())
    }

    override fun initListener() {
        super.initListener()
        binding.tvHaveAccount.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.btnSignUp.setOnClickListener {
            if (validateFields()) {
                val email = binding.etEmailSignUp.text.toString()
                val pass = binding.etPasswordSignUp.text.toString()
                val confirmPass = binding.etAcceptPasswordSignUp.text.toString()

                if (email.isNotEmpty() && pass.isNotEmpty() && confirmPass.isNotEmpty()) {
                    if (pass == confirmPass) {

                        firebaseAuth.createUserWithEmailAndPassword(email, pass)
                            .addOnCompleteListener {
                                if (it.isSuccessful) {
                                    pref.setFistName(binding.etFirstName.text.toString())
                                    pref.setLastName(binding.etLastName.text.toString())
                                    pref.setGender(binding.etGenderSignUp.text.toString())
                                    pref.setAge(binding.etAgeSignUp.text.toString())
                                    pref.setEmail(binding.etEmailSignUp.text.toString())

                                    findNavController().navigate(R.id.onBoardingFragment)
                                } else {
                                    Toast.makeText(
                                        requireContext(),
                                        it.exception.toString(),
                                        Toast.LENGTH_SHORT
                                    ).show()

                                }
                            }
                    } else {
                        Toast.makeText(requireContext(), "Пароли не совподают!", Toast.LENGTH_SHORT)
                            .show()
                    }
                } else {
                    Toast.makeText(requireContext(), "Заполните поля!", Toast.LENGTH_SHORT).show()

                }
            }
        }

        binding.tvCreateAccountSignIn.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.etGenderSignUp.isFocusable = false
        binding.etGenderSignUp.setOnClickListener {
            showGenderDialog()
        }

        binding.etAgeSignUp.isFocusable = false
        binding.etAgeSignUp.setOnClickListener {
            showAgeDialog()
        }

    }

    private fun validateFields(): Boolean {
        val email = binding.etEmailSignUp.text.toString().trim()
        val password = binding.etPasswordSignUp.text.toString().trim()
        val confirmPassword = binding.etAcceptPasswordSignUp.text.toString().trim()
        val firstName = binding.etFirstName.text.toString().trim()
        val lastName = binding.etLastName.text.toString().trim()
        val gender = binding.etGenderSignUp.text?.trim().toString()
        val age = binding.etAgeSignUp.text?.trim().toString()

        if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() || firstName.isEmpty()) {
            Toast.makeText(
                requireContext(),
                "Пожалуйста, заполните все обязательные поля",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        }

        val emailPattern = "[a-zA-Z0-9._-]+@gmail.com"
        if (!email.matches(emailPattern.toRegex())) {
            binding.etEmailSignUp.error = "Пожалуйста, введите действительный адрес Gmail"
            return false
        }

        if (password.length < 8) {
            Toast.makeText(
                requireContext(),
                "Пароль должен содержать не менее 8 символов",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }

        if (password != confirmPassword) {
            Toast.makeText(requireContext(), "Пароли не совпадают", Toast.LENGTH_SHORT).show()
            return false
        }

        val namePattern = "^[А-ЯЁA-Z][а-яА-ЯёЁa-zA-Z]*$"
        if (!firstName.matches(namePattern.toRegex())) {
            Toast.makeText(
                requireContext(),
                "Имя должно содержать только буквы и начинаться с заглавной буквы",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }

        if (gender.isEmpty()) {
            Toast.makeText(requireContext(), "Выберите пол", Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }

    fun showGenderDialog() {
        val genderOptions = arrayOf("Мужчина", "Женщина", "Другой")

        val alertDialog = AlertDialog.Builder(requireContext())
            .setTitle("Выберите пол: ")
            .setItems(genderOptions) { dialog, which ->
                binding.etGenderSignUp.setText(genderOptions[which])
                dialog.dismiss()
            }
            .create()

        alertDialog.show()
    }

    fun showAgeDialog() {
        val ageOptions = Array(81) { (it + 10).toString() }

        val alertDialog = AlertDialog.Builder(requireContext())
            .setTitle("Выберите возраст")
            .setItems(ageOptions) { dialog, which ->
                val selectedOption = ageOptions[which]
                binding.etAgeSignUp.setText(selectedOption)
                dialog.dismiss()
            }
            .create()

        alertDialog.show()
    }
}