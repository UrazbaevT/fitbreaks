package com.example.fitbreak_pres.ui.warm_up.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import com.example.fitbreak_pres.data.model.WarmUp
import com.example.fitbreak_pres.databinding.ItemWarmUpBinding

class WarmUpAdapter(private val list: ArrayList<WarmUp>, private val onClick: (WarmUp) -> Unit, private val onMeditationClick: (WarmUp) -> Unit) :
    Adapter<WarmUpAdapter.WarmUpViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WarmUpViewHolder {
        return WarmUpViewHolder(
            ItemWarmUpBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: WarmUpViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class WarmUpViewHolder(private val binding: ItemWarmUpBinding) :
        ViewHolder(binding.root) {
        fun bind(warmUp: WarmUp?) {
            binding.imgItem.load(warmUp?.image)
            binding.tvTitle.text = warmUp?.title

            itemView.setOnClickListener {
                onClick(warmUp!!)
            }

            if (adapterPosition == list.lastIndex) {
                itemView.setOnClickListener {
                    onMeditationClick(warmUp!!)
                }
            } else {
                itemView.setOnClickListener {
                    onClick(warmUp!!)
                }
            }
        }

    }
}