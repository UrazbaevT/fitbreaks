package com.example.fitbreak_pres.core.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.fitbreak_pres.R

fun ImageView.loadWarmup(image: String?){
    Glide.with(this).load(image).into(this)
}

fun ImageView.loadImage(image: String?){
    Glide.with(this).load(image).placeholder(R.drawable.img_avatar).into(this)
}