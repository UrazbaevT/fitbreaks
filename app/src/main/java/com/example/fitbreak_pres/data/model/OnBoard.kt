package com.example.fitbreak_pres.data.model

import java.io.Serializable

data class OnBoard(
    val title: String,
    val desc: String,
    val img: Int,
    val radioFirst: String,
    val radioSecond: String,
    val radioThird: String,
    val radioFourth: String,
    val radioFifth: String,
    val btnRadioFifth: Boolean,
) : Serializable