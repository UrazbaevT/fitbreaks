package com.example.fitbreak_pres.data.local

import android.content.Context
import android.content.Context.MODE_PRIVATE

class Pref(private val context: Context) {

    private val pref = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE)

    //onBoarding
    fun isUserSeen(): Boolean{
        return pref.getBoolean(SEEN_KEY, false)
    }

    fun saveSeen(){
        pref.edit().putBoolean(SEEN_KEY, true).apply()
    }

    //age
    fun saveAge(age: String){
        pref.edit().putString(AGE_KEY, age).apply()
    }

    fun getAge(): String{
        return pref.getString(AGE_KEY, "").toString()
    }

    //login
    fun saveLogin(login: String){
        pref.edit().putString(LOGIN_KEY, login).apply()
    }

    fun getLogin(): String{
        return pref.getString(LOGIN_KEY, "").toString()
    }

    //FirstName
    fun saveFirstName(firstName: String){
        pref.edit().putString(FIRST_NAME_KEY, firstName).apply()
    }

    fun getFirstName(): String{
        return pref.getString(FIRST_NAME_KEY, "").toString()
    }

    //LastName
    fun saveLastName(lastName: String){
        pref.edit().putString(LAST_NAME_KEY, lastName).apply()
    }

    fun getLastName(): String{
        return pref.getString(LAST_NAME_KEY, "").toString()
    }

    //image
    fun saveImage(image: String){
        pref.edit().putString(IMAGE_KEY, image).apply()
    }

    fun getImage(): String?{
        return pref.getString(IMAGE_KEY, "")
    }

    //RadioButton
    fun saveRadioButton(selectedOption: String) {
        pref.edit().putString("selected_option", selectedOption).apply()
    }

    fun getRadioButton(): String {
        return pref.getString("selected_option", "") ?: ""
    }

    companion object{
        const val PREF_NAME = "Task.name"
        const val SEEN_KEY = "seen.key"
        const val AGE_KEY = "age.key"
        const val LOGIN_KEY = "login.key"
        const val FIRST_NAME_KEY = "first.name.key"
        const val LAST_NAME_KEY = "last.name.key"
        const val IMAGE_KEY = "image.key"
    }
}