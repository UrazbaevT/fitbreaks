package com.example.fitbreak_pres.data.model

data class MeditationX(
    val Meditation: List<Meditation>? = null
)

data class Meditation(
    val author: String? = null,
    val image: String? = null,
    val song: String? = null,
    val link: String? = null,
    var isPlaying: Boolean = false,
    var isCurrentSongPlaying: Boolean = false
)