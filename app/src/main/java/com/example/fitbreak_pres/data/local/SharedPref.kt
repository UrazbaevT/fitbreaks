package com.example.fitbreak_pres.data.local

import android.content.Context
import android.content.SharedPreferences


class SharedPref (context: Context) {

   private val sharedPref: SharedPreferences = context.getSharedPreferences("presences", Context.MODE_PRIVATE)

    fun isFistName(): String {
        return sharedPref.getString("firstName", "").toString()
    }

    fun setFistName(isSnow: String) {
        sharedPref.edit().putString("firstName", isSnow).apply()
    }

    fun isLastName(): String {
        return sharedPref.getString("lastName", "").toString()
    }

    fun setLastName(isSnow: String) {
        sharedPref.edit().putString("lastName", isSnow).apply()
    }

    fun isGender(): String {
        return sharedPref.getString("gender", "").toString()
    }

    fun setGender(isSnow: String) {
        sharedPref.edit().putString("gender", isSnow).apply()
    }

    fun isAge(): String {
        return sharedPref.getString("age", "").toString()
    }

    fun setAge(isSnow: String) {
        sharedPref.edit().putString("age", isSnow).apply()
    }

    fun isEmail(): String {
        return sharedPref.getString("email", "").toString()
    }

    fun setEmail(email: String) {
        sharedPref.edit().putString("email", email).apply()
    }

}