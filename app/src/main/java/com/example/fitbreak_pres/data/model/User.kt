package com.example.fitbreak_pres.data.model

data class User(
    val firstName: String? = "-",
    val lastName: String? = "-",
    val gender: String? = "-",
    val age: String? = "-",
    val email: String? = null
)