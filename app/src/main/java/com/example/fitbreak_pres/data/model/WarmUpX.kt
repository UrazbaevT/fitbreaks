package com.example.fitbreak_pres.data.model

import java.io.Serializable

data class WarmUpX(
    val WarmUp: List<WarmUp>? = null
)

data class WarmUp(
    val detail: Detail? = null,
    val image: String? = null,
    val title: String? = null
) : Serializable

data class Detail(
    val desc1: String? = null,
    val desc2: String? = null,
    val howMany1: String? = null,
    val howMany2: String? = null,
    val image1: String? = null,
    val image2: String? = null,
    val title1: String? = null,
    val title2: String? = null
)